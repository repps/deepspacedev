﻿// Created by Rashid Epps

/*
	This script is used to animate Dev's ship and tell the
	game controller when the player has beaten a level.
	The ship moves backwards a bit, then forwards until it is
	off the screen.
*/


using UnityEngine;
using System.Collections;

public class DevShip : MonoBehaviour {

	private GameController gameController;
	private Animator animator;
	private Renderer shipRenderer;

	private bool win;
	private bool moveForward = false;
	private Vector3 backwards;

	void Start ()
	{
		// Create a vector representing the backwards
		backwards = new Vector3(transform.position.x - 1, transform.position.y, transform.position.z);

		// Grab the animator component
		animator = GetComponent<Animator>();

		// Grab the game controller
		gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();

		// Grab the renderer of the ship
		shipRenderer = GetComponent<Renderer>();
	}


	void OnTriggerEnter2D(Collider2D other) {

		// If the player comes into contact with the ship, then set the win boolean to true
		if (other.tag == "Player") {

			win = true;

			//Set the trigger animation and delete the player
			animator.SetTrigger("devEnterShip");

			//GameObject.Destroy (other.gameObject);
			other.gameObject.SetActive(false);

			//display the win text
			gameController.WinLevel();
		}

	}

	void Update ()
	{
		// If the win boolean is true, begin to move the ship backwards
		if (win)
		{
			transform.position = Vector3.MoveTowards(transform.position, backwards, Time.deltaTime);
		}

		// If the ship has gone backwards far enough, begin to move forwards
		// Set the boolean moveForward to true
		if (transform.position == backwards)
		{
			moveForward = true;
			win = false;
		}

		// Begin moving forward if true
		if (moveForward)
		{
			transform.Translate(transform.right * Time.deltaTime * 7);

			// If the ship is not rendered, go to the next level screen
			if (!shipRenderer.isVisible)
			{
				Application.LoadLevel("NextLevelScreen");
			}
		}
	}
}
