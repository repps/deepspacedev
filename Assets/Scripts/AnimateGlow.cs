﻿// Created by Rashid Epps

/*
	This script is used to animate the glow around 
	Dev's ship. The amount of time that the glow animation
	plays.
*/


using UnityEngine;
using System.Collections;

public class AnimateGlow : MonoBehaviour {

	public float glowTime;
	
	// Update is called once per frame
	void Update () {

		// Fluctuate the scale of the object along with our time
		if (Time.timeSinceLevelLoad < glowTime)
		{
			transform.localScale = new Vector3(Mathf.PingPong(Time.time, 1.3f), Mathf.PingPong(Time.time, 1.3f), 1);
		}
		else
		{
			gameObject.SetActive(false);
		}


	
	}
}
