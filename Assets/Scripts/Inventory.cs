﻿/* Created by Eric Poretsky */

/* Used to create the inventory/inventory functionality/GUI. Allows for stackable items */
/* as well as using or adding items to the inventory */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {

	private RectTransform inventoryRect;

	private float inventoryWidth, inventoryHeight;

	public int slots;

	public int rows;

	public float slotPaddingLeft, slotPaddingTop;

	public float slotSize;
	private float slotWidth;
	private float slotHeight;

	public GameObject slotPrefab;

	private List<GameObject> allSlots;

	private static int emptySlot;

	public static int EmptySlot {
		get { return emptySlot; }
		set { emptySlot = value;}
	}
		
	void Start () {
		CreateLayout ();
	}
		
	/* Creates the layout for the inventory GUI by calculating the width/height
	 based off of how many slots or rows are specified. Allows for a dynamic inventory based
	 off how many rows/slots are specified. */
	private void CreateLayout() {

		slotSize = (slotSize/Screen.height) * 500;

		allSlots = new List<GameObject> ();

		//might need to change this 
		emptySlot = slots;

		inventoryWidth = (slots / rows) * (slotSize + slotPaddingLeft) + slotPaddingLeft;

		inventoryHeight = rows * (slotSize + slotPaddingTop) + slotPaddingTop;

		inventoryRect = GetComponent<RectTransform> ();

		inventoryRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, inventoryWidth);
		inventoryRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, inventoryHeight);

		int columns = slots / rows;

		for (int y = 0; y < rows; y++) {

			for (int x = 0; x < columns; x++) {

				GameObject newSlot = (GameObject)Instantiate(slotPrefab);

				RectTransform slotRect = newSlot.GetComponent<RectTransform>();

				newSlot.name = "Slot";

				newSlot.transform.SetParent(this.transform.parent);

				slotRect.localPosition = inventoryRect.localPosition + new Vector3 (slotPaddingLeft * (x + 1) + (slotSize * x), -slotPaddingTop * (y + 1) - (slotSize*y) );

				slotRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, slotSize);
				slotRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, slotSize);

				allSlots.Add (newSlot);
			}
		}
	}


	/* Add item functionality for the inventory. Checks to see if the slot is stackable (has a max size > 1)
	, otherwise loops through the slots of the inventory, finds an empty slot, and adds it to the slot/inventory */
	public bool AddItem(Item item) {

		//Checks to see if an item is not stackable
		if (item.maxSize == 1) {
			PlaceEmpty (item);
			return true;
		} else {

			foreach (GameObject slot in allSlots) {

				Slot temp = slot.GetComponent<Slot> ();

				if (!temp.isEmpty) {

					//checks for stackability (not implemented)
					if (temp.CurrentItem.type == item.type) {
						temp.AddItem (item);
						emptySlot--;
						return true;
					}
				}
			}

			if (emptySlot > 0) {
				PlaceEmpty (item);
			}
		}

		//returns false if item was unable to be added
		return false;
	}

	//Looks for an empty slot in the slot's array and adds an item if empty
	private bool PlaceEmpty(Item item) {

		if (emptySlot > 0) {
			foreach (GameObject slot in allSlots) {
				Slot temp = slot.GetComponent<Slot> ();
				if (temp.isEmpty) {
					temp.AddItem (item);
					emptySlot--;
					return true;
				}
			}
		}

		//returns false if no empty slots exist
		return false;
	}
		
}
