﻿/* Created by Eric Poretsky with additional coding by Rashid Epps */

/* Creates an enumerable type allowing for different items with varying functionalities
Also implements a click and drag functionality for moving planets out of the inventory */

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public enum ItemType {PLANET};

public class Item : MonoBehaviour {

	public ItemType type;

	public Sprite spriteNeutral;

	public Sprite spriteHighlighted;

	public int maxSize;

	private bool drag = false;
	private GameController gameController;
	private ObjectFollow objectFollow;
	private Mover mover;

	void Start ()
	{
		mover = GetComponent<Mover>();
		objectFollow = GetComponent<ObjectFollow>();
		gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
	}

	/* Update method allows for planets to be clicked and dragged around the screen */
	void Update ()
	{
		//Dragging the planet to a specified location from the slot
		//Drag is set to true only after popping it from the slot, then when mouse is released, set to false.
		if (drag == true)
		{
			gameController.DisableCameraDrag();
			Vector2 mousePosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			Vector2 objPosition = Camera.main.ScreenToWorldPoint (mousePosition);
			transform.position = objPosition;
			objectFollow.OnMouseDrag();
		}
		//After releasing the planet check if it is too close and re enable camera
		if (Input.GetMouseButtonUp(0))
		{
			drag = false;
			mover.OnMouseUp();
			gameController.EnableCameraDrag();
		}

	}
		
	/* Called when a slot/item is pressed */
	public void Use() {
		switch (type) {
		case ItemType.PLANET:
			gameObject.SetActive (true);
			drag = true;		
			break;
		}
	}
}
