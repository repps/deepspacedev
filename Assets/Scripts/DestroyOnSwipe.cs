﻿// Created by Rashid Epps and Eric Poretsky

/*
	This script is used to destroy the swipe animation when 
	the player has successfully interacted with the object.
*/


using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DestroyOnSwipe : MonoBehaviour {

	public float destroyTime;									// The time to which the object will be destroyed
	private Animator animator;
	private GameObject arrow;
	private Vector3 translate = new Vector3 (-5f, 2.25f, 0f);


	// Use this for initialization
	void Start () {
		
		// Grab the animator components upon start
		animator = gameObject.GetComponent<Animator>() ;
		arrow = GameObject.Find ("Arrow");
		arrow.SetActive (false);
		animator.enabled = false;

	}
	
	// Update is called once per frame
	void Update () {

		// Translate the hand object until it reaches our inventory
		// Then set the animation to active
		if (transform.position.x > -8.5f) { 
			transform.Translate (translate * 0.009f);

			if (transform.position.x == -8.50501f) {
				arrow.SetActive (true);
				animator.enabled = true;
			}
		}

		// Check whether enough time has passed to destroy the object
		if (Time.timeSinceLevelLoad > destroyTime)
		{
			GameObject.Destroy(gameObject);
		}

		// Check whether the player has interacted with the screen, then destroy
		if (Input.GetMouseButtonDown(0))
		{
			if (EventSystem.current.IsPointerOverGameObject())
			{
				GameObject.Destroy(gameObject);
			}
		}


	
	}
}
