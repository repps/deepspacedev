﻿// Created by Rashid Epps

// *********CURRENTLY NOT USED********

/*
	This script is used to fade the dots in the trajectory path in and out
*/

using UnityEngine;
using System.Collections;
 
public class FadeObjectInOut : MonoBehaviour
{

	private SpriteRenderer[] dots;
	private Color lerpedColor;
	private bool drawn = false;

	void Start ()
	{
		// Grab all renderers in the dots object
		dots = GetComponentsInChildren<SpriteRenderer>();
	}

	void Update ()
	{
		// Only run this code once if dots are not instantiated
		if (dots.Length == 0 && !drawn)
		{
			drawn = true;
			dots = GetComponentsInChildren<SpriteRenderer>();

		} 
        
		// Fade the color of the dots from black to white
        lerpedColor = Color.Lerp(Color.white, Color.black, Mathf.PingPong(Time.time, 1));

        foreach (SpriteRenderer renderer in dots)
        {
        	renderer.color = lerpedColor;
		}
	} 
 
}