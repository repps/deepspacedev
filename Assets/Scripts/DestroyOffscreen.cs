﻿/* Created by Eric Poretsky with additional coding by Rashid Epps */

/* Script for destroying and reinstantiating Dev when offscreen (dead) */
/* For educational use only */

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class DestroyOffscreen : MonoBehaviour {

	private GameController gameController;
	private Renderer devR;
	private Rigidbody2D dev;
	private Pause pause;
	private float nextReset;
	private AudioSource audioSource;
	public AudioClip offscreenClip;

	private bool rendered = false;

	private GameObject[] jetPackIcons;

	void Start() {

		audioSource = GetComponent<AudioSource>();

		//Assign gameController
		gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();

		//get dev's renderer
		devR = GetComponent<Renderer> ();

		//get dev's rigidbody
		dev = GetComponent<Rigidbody2D> ();

		//Game pause object
		GameObject pauseObject = GameObject.FindWithTag("PauseScreen");
        if (pauseObject != null) 
        {
            pause = pauseObject.GetComponentInChildren <Pause>();
        }

		jetPackIcons = GameObject.FindGameObjectsWithTag ("JetIcon");
	}

	// Update is called once per frame
	void Update () {
		
		//check to see if dev has been rendered
		if (devR.isVisible == true) {
			rendered = true;
		}

		//if dev is out of the screen, destroy dev and replace him
		if ((devR.isVisible == false) && (rendered == true) && (!pause.isPaused))  {
				Delay ();
			}
		
	}

	IEnumerator ResetPlayer() {

		if (Time.time > nextReset)
		{
			nextReset = Time.time + 1.5f;
			audioSource.PlayOneShot(offscreenClip, 0.2f);

			//delay .5 sec and reload scene
			yield return new WaitForSeconds(1.5f);

			GameObject.Destroy (GameObject.FindGameObjectWithTag ("Player"));
			gameController.UpdateDeath();

			//OLD CODE
			// SceneManager.LoadScene(SceneManager.GetActiveScene().name);

			//Reset the SunCollider
			CircleCollider2D sunCol = GameObject.FindWithTag ("Sun").GetComponent<CircleCollider2D> ();
			sunCol.enabled = true;

			//Reinstantiate Dev and set his Rigidbody to the beginning position
			GameObject devC = (GameObject)Instantiate(Resources.Load("Dev"), gameController.GetStartPosition(), Quaternion.identity);
			dev = devC.GetComponent<Rigidbody2D>();

			foreach (GameObject jet in jetPackIcons) {
				jet.SetActive (true);
			}

		}
	
	}


	void Delay () {
			//Coroutine for Delay Action
			StartCoroutine (ResetPlayer ());
	}





}
