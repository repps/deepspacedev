﻿/* Created by Eric Poretsky with additional coding by Rashid Epps */

/* Allows objects to be moveable, a click and hold functionality to add them to the inventory,
and checking to make sure two objects are not placed too close to eachother */

/* Attach this script to an object to be able
* to move it within the game. */

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Mover : MonoBehaviour 
{
	private GameController gameController;

	private Pause pause;
	private Button pauseButton;
	private GameObject pauseObject;

	private bool objectsError;
	private GUIStyle style = GUIStyle.none;

	private Inventory inventory;

	private Collider2D planet;

	private float currentTime;
	private float lastTime;
	private float deltaTime;
	private float accumTime;

	private bool drag = true;

	void Start () 
	{
		inventory = GameObject.FindWithTag("Inventory").GetComponent<Inventory>();

		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}

		pauseObject = GameObject.FindWithTag("PauseScreen");
		pauseButton = GameObject.Find("Pause/Play").GetComponent<Button>();
		if (pauseObject != null) 
		{
			pause = pauseObject.GetComponentInChildren <Pause>();
		}

		planet = GetComponent<Collider2D> ();
	}

	/* Calculates the passing time even if the game is paused */
	void Update () {
		currentTime = Time.realtimeSinceStartup;

		// If the game is paused set the layer mask to default
		// If not, set the layer to ignore raycast
		// This is so that we can still raycast to check if objects are too close
		if (pause.isPaused)
		{
			gameObject.layer = LayerMask.NameToLayer("Default");
		}
		else
		{
			gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
		}
	}
		
	/* Dragging functionality for planets, also allows a planet to be added to the inventory
	if the user clicks and holds */
	public void OnMouseDrag()
	{
		//A player should only be able to move planets if Dev is orbiting the sun
		if (!EventSystem.current.IsPointerOverGameObject() && gameController.isOrbiting()) {

			//If the player starts dragging a planet the game should get paused
			pause.isPaused = true;
			lastTime = Time.realtimeSinceStartup;

			//if the item is being dragged continuously move it's position in relation to the mouse
			if (drag == true) {

				deltaTime = lastTime - currentTime;
				accumTime += deltaTime;

				Vector2 mousePosition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
				Vector2 objPosition = Camera.main.ScreenToWorldPoint (mousePosition);

				if (transform.position.x == objPosition.x && transform.position.y == objPosition.y && accumTime > 1.0f) {
					drag = false;

				} else if (transform.position.x != objPosition.x && transform.position.y != objPosition.y) {
					drag = true;
					accumTime = 0.0f;
				}

				transform.position = objPosition;

			/*if an item is not being dragged check to see if it's being held, and if so
			 add it to the inventory */
			} else if (drag == false) {

				deltaTime = lastTime - currentTime;
				accumTime += deltaTime;

				//how long a player is allowed to hold the item
				// after popping the item to the inventory set pause off
				if (accumTime > 1.0f) {
					inventory.AddItem (planet.GetComponent<Item> ());

					pause.isPaused = false;

					gameObject.SetActive (false);
					accumTime = 0.0f;
					drag = true;
					pauseObject.SetActive(true);
				}
			} 
		}

	}

	/* Checks to see if a player placed an object too close to another */
	public void OnMouseUp()
	{
		if (pause.isPaused)
		{
			accumTime = 0.0f;
			drag = true;

			float radius = GetComponent<CircleCollider2D>().radius * transform.localScale.x;

			Collider2D[] colliders = Physics2D.OverlapCircleAll((Vector2)transform.position, radius, Physics2D.DefaultRaycastLayers);

			//If the planet collider is touching other objects in the scene, disable pause button
			if (colliders.Length >= 3)
			{
				objectsError = true;
				pauseObject.SetActive(false);
			}
			else
			{
				//unpause the screen after object is succesfully placed
				objectsError = false;
				pauseObject.SetActive(true);
			}
		}

	}

	// Error message if planet is placed too close to another
	void OnGUI()
	{
		style.font = (Font) Resources.Load("Fonts/Msyi");
		style.fontSize = 50;
		style.alignment = TextAnchor.UpperLeft;
		style.normal.textColor = Color.white;
		if (objectsError)
		{
			GUI.Label(new Rect(0.25f * Screen.width, 0.25f * Screen.height, 0.5f * Screen.width, 0.5f * Screen.height), "Objects too close!", style);
		}
	}
}