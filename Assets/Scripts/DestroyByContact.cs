// Created by Rashid Epps

/*
	This script is used to destroy the player when they come in
	contact with this object's collider. It plays the explosion
	animation, sound, and resets the player's position
*/


using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {

	private GameController gameController;
	private Rigidbody2D dev;
	private GameObject explosionObject;
	private AudioSource explosionSFX;

	private GameObject[] jetPackIcons;


	// Use this for initialization
	void Start () {
		// Grab the game controller object
		gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();

		explosionSFX = GetComponent<AudioSource>();

		//Get the rigidbody of dev and the moon
		dev = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D> ();
		jetPackIcons = GameObject.FindGameObjectsWithTag ("JetIcon");
	}
	
	// Update is called once per frame
	void Update () {

		//After dev dies, regrab his rigidbody
		if (dev == null) {
			dev = GameObject.FindWithTag ("Player").GetComponent<Rigidbody2D> ();
		}

		//Destroy the Explosion object if it is done playing
		if (explosionObject != null && !explosionObject.GetComponent<ParticleSystem>().isPlaying)
		{
			GameObject.Destroy (explosionObject);
		}
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{
			// If the player enters the collider, destroy and play the animation
			if (other.tag == "Player")
			{
				explosionSFX.Play();
				explosionObject = (GameObject)Instantiate(Resources.Load("Explosion"), dev.position, dev.transform.rotation);
				GameObject.Destroy (GameObject.FindGameObjectWithTag ("Player"));
				gameController.UpdateDeath();



				//Re enable the sun collider
				CircleCollider2D sunCol = GameObject.FindWithTag ("Sun").GetComponent<CircleCollider2D> ();
				sunCol.enabled = true;
				
				foreach (GameObject jet in jetPackIcons) {
					jet.SetActive (true);
				}

				//Re-Loading Dev from the resource folder 
				GameObject devC = (GameObject)Instantiate(Resources.Load("Dev"), gameController.GetStartPosition(), Quaternion.identity);
				dev = devC.GetComponent<Rigidbody2D>();
			}
			
			// If a comet enters the collider destroy it
			if (other.tag == "Comet")
			{
				explosionSFX.Play();
				explosionObject = (GameObject)Instantiate(Resources.Load("Explosion"), other.transform.position, other.transform.rotation);
				GameObject.Destroy(other.gameObject);
	
			}
	}
}
