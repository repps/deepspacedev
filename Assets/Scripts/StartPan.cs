﻿// Created by Rashid Epps

/*
	This script pans the camera at the beginning of a level
	so that the player can view the goal of the stage. Then 
	the script turns on the other camera scripts afterwards.
*/

using UnityEngine;
using System.Collections;

public class StartPan : MonoBehaviour {

	private GameObject rightBoundary;
	private GameObject leftBoundary;
	private Vector3 startPosition;
	private Vector3 endPosition;
	private float moveTowards;

	public float panSpeed;
	public float waitTime;

	// Use this for initialization
	void Start () {

		rightBoundary = GameObject.FindWithTag("Right");
		leftBoundary = GameObject.FindWithTag("Left");

		startPosition = rightBoundary.transform.position;
		endPosition = leftBoundary.transform.position;
		
		transform.position = new Vector3(startPosition.x, transform.position.y, transform.position.z);
	
	}
	
	// Update is called once per frame
	void Update () {

		//If press screen skip the pan
		if (Input.GetMouseButtonDown(0))
		{
			GetComponentInChildren<DevFollow>().enabled = true;
			GetComponentInChildren<StartPan>().enabled = false;
		}

		if ((Time.timeSinceLevelLoad + 7) > waitTime)
		{
			//Move the x component of the camera towards the right boundary of our level
			moveTowards = Mathf.MoveTowards(transform.position.x, leftBoundary.transform.position.x, panSpeed * Time.deltaTime);
			transform.position = new Vector3(moveTowards, 0.0f, -10.0f);
		}

		//If the camera has reached the boundary, wait some time, then reset the camera
		if (transform.position.x == endPosition.x)
		{
			transform.position = new Vector3(endPosition.x, transform.position.y, transform.position.z);
			GetComponentInChildren<DevFollow>().enabled = true;
			GetComponentInChildren<StartPan>().enabled = false;

		}
	}
}
