﻿/* Created by Eric Poretsky */

/* Script for toggling the UI Menu on and off. Since our game implements a pause and a menu 
button (both which pause the game's time), the menu and pause objects must know eachother's states*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuToggle : MonoBehaviour {

	public bool display;

	private GameObject menuPanel;

	private Button pauseButton;
	private GameObject pauseObject;
	private Pause pause;

	void Start () {
		menuPanel = GameObject.Find ("MenuPanel");
		pauseButton = GameObject.Find("Pause/Play").GetComponent<Button>();
		pauseObject = GameObject.Find ("Pause/Play");
		pause = GameObject.FindWithTag("PauseScreen").GetComponent<Pause>();
		menuPanel.SetActive (false);
		display = false;
	}
	
	/* Displays the menu while making sure the player can not also click the pause button.
	*/
	void Update () {

		//if the menu is active the player should not be able to click the pauseButton
		if (display == true) {
			menuPanel.SetActive (true);
			pauseButton.interactable = false;
		//if the menu is not active and the pauseObject is not visible (i.e. planets are not moving)
		} else if ((display == false) && (!pauseObject.activeInHierarchy)) {
			menuPanel.SetActive (false);
			pauseButton.interactable = true;
		//if the menu is not active and the pauseObject is visible
		} else if (display == false) {
			menuPanel.SetActive (false);
			pauseButton.interactable = true;
		}
	}

	/* Toggles the menu on and off */
	public void displayMenu() {
		display = !display;
		if (display == true)
		{
			pause.isPaused = true;
		}
		else
		{
			pause.isPaused = false;
		}
	}

}
