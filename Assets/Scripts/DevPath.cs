﻿// Created by Rashid Epps

/*
	This DevPath script is used to calculate and show
	Dev's projected path using dots, and how they will
	be affected by the gravity of the planets
*/


using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class DevPath : MonoBehaviour {

	private GameObject ui;
	public bool drawDots;					//boolean that is used to control rendering of the dots

	private Vector2 gravityForce;
	private Transform devPosition;
	private GameObject[] planets;
	private GameController gameController;
	private Collider2D sunCollider;

	public int numberOfPoints;
	public GameObject dash;
	
	private Vector2 initialVelocity;
	private Vector2 newVelocity;
	private GameObject[] dashes;

	void Start ()
	{
		//Checks the ui for the draw dots toggle
		ui = GameObject.Find("UI");
		drawDots = ui.GetComponentInChildren<Toggle>(true).isOn;

		//Save Dev start position
		devPosition = gameObject.transform;

		//Get dots parent
		Transform dots = transform.GetChild(1);

		//Instantiate all of the dots
		dashes = new GameObject[numberOfPoints];
		for (int i=0; i<numberOfPoints; i++)
		{
			dash = (GameObject) Instantiate(Resources.Load("Dot"));
			dash.transform.parent = dots;
			dash.GetComponent<Renderer>().enabled = false;
			dashes[i] = dash;
		}

		//Get the suncollider to tell if dev has left the sun or not
		sunCollider = GameObject.FindWithTag("Sun").GetComponent<CircleCollider2D>();

		//Grab all planets in scene
		planets = GameObject.FindGameObjectsWithTag("Planet");

		//Grab the game controller object
		gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();
	}
		

	void Update ()
	{
		//This always grabs the drawDots boolean so that it updates in game
		drawDots = ui.GetComponentInChildren<Toggle>(true).isOn;

		//If draw dots is enabled and dev has not left the sun.
		if ((drawDots == true) && sunCollider.enabled)
		{
			if (devPosition == null)
			{
				devPosition = GameObject.FindWithTag("Player").transform;
			}

			Vector2 devVelocity = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D>().transform.up;
			setTrajectoryPoints ((Vector2)devPosition.position, devVelocity);
		}
		//Else once Dev leaves the sun turn off the dots
		else
		{
			foreach (GameObject dash in dashes)
			{
				dash.GetComponent<Renderer>().enabled = false;
			}
		}

	}


	//This function calculates the attraction force between dev and a planet
	Vector2 Attract(Rigidbody2D planet, Vector2 dotPosition)
	{
		//Get the distance between dev and the planet
		Vector2 force = (planet.position - dotPosition);
		float distance = force.magnitude;

		//distance = Mathf.Clamp(distance, 2.0f, 100.0f);

		//Change force to a unit vector and calculate the strength using the gravity formula
		force.Normalize();
		float strength = ((0.4f * planet.mass * 20.0f) / (distance * distance));
		force *= strength;

		return force;
	}

	//This function sets the position of each dot that is in the projected path
	void setTrajectoryPoints (Vector2 startPosition, Vector2 startVelocity)
	{
		//Initialize all of our variables for the position equation
		float fTime = 0.0f;
		fTime += Time.fixedDeltaTime;
		Vector2 pos = Vector2.zero;
		Vector2 velocity = startVelocity * 12.0f;

		float distance = 0.0f;

		foreach (GameObject dash in dashes)
		{
			//Reset the gravity force for each dot
			gravityForce = new Vector2 (0.000000f, 0.000000f);

			//Go through all of the planets and check whether the dot is within range to be affected
			foreach (GameObject planet in planets)
			{
				//Calculate relative position
				Vector2 relativePos = (Vector2)planet.transform.position - (pos + startPosition);

				//Based on the relative vector, this angle makes sure that Dev is perpendicular to the vector
				float angle = Mathf.Atan2(relativePos.y, relativePos.x) * Mathf.Rad2Deg;

				//Compute distance and gravitational force
				distance = ((Vector2)planet.transform.position - (pos + startPosition)).magnitude;
				if (distance < planet.GetComponent<PlanetGravity>().range)
				{
					gravityForce = Attract(planet.GetComponent<Rigidbody2D>(), (pos + startPosition))/20;
				}
				else
				{
					continue;
				}
			}

			//Update our position equation and set the dot to be rendered
			velocity += gravityForce * fTime;
			pos += velocity * fTime * fTime;
			dash.transform.position = pos + startPosition;
			dash.GetComponent<Renderer>().enabled = true;
			fTime += Time.fixedDeltaTime;
		}


	}

}
