﻿/* Created by Eric Poretsky with additional coding by Rashid Epps */

/* Slot Prefab that initializes the empty slots/empty slot's layout for the inventory
Includes functionality for adding item's to a slot and changing the sprite accordingly */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IPointerDownHandler {

	private Mover mover;
	private Pause pause;
	private GameController gameController;
	
	private Stack<Item> items;

	public Text stackTxt;

	public Sprite slotEmpty;
	public Sprite slotHighlight;

	//checks if the slot is empty
	public bool isEmpty {
		get {return items.Count == 0;}
	}

	//checks if the slot is stackable
	public bool IsAvailable {
		get {return CurrentItem.maxSize > items.Count;}
	}

	//returns the top item of a slot (if stackable)
	public Item CurrentItem {
		get {return items.Peek (); }
	}

	//Creates the GUI slot layout
	void Start () {
		gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();

		GameObject pauseObject = GameObject.FindWithTag("PauseScreen");
        if (pauseObject != null) 
        {
            pause = pauseObject.GetComponentInChildren <Pause>();
        }
	
		items = new Stack<Item> ();
		RectTransform slotRect = GetComponent<RectTransform> ();

		RectTransform txtRect = stackTxt.GetComponent<RectTransform> ();

		int txtScaleFactor = (int)(slotRect.sizeDelta.x * 0.60);
		stackTxt.resizeTextMaxSize = txtScaleFactor;
		stackTxt.resizeTextMinSize = txtScaleFactor;
	

		txtRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Vertical, slotRect.sizeDelta.y);
		txtRect.SetSizeWithCurrentAnchors (RectTransform.Axis.Horizontal, slotRect.sizeDelta.x);

	}

	//Adds an item to the slot and changes the sprite accordingly (works for stackability)
	public void AddItem (Item item) {

		items.Push (item);

		if (items.Count > 1) {
			stackTxt.text = items.Count.ToString ();
		}
		ChangeSprite (item.spriteNeutral, item.spriteHighlighted);
	}

	//Changes the slot sprite to the new item (both neutral and highlighted)
	public void ChangeSprite(Sprite neutral, Sprite highlight)
	{
		GetComponent<Image> ().sprite = neutral;

		SpriteState st = new SpriteState ();

		st.highlightedSprite = highlight;

		st.pressedSprite = neutral;

		GetComponent<Button>().spriteState = st;
	}

	//Uses a planet from the pressed slot (works with stackability)
	// and changes the sprite back to empty if the slot is empty
	private void PlanetUse () {

		if (!isEmpty) {
			
			items.Pop ().Use (); 

			stackTxt.text = items.Count > 1 ? items.Count.ToString () : string.Empty;

			if (isEmpty) {
				ChangeSprite (slotEmpty, slotHighlight);
				Inventory.EmptySlot++;
			}
		}
	}

	//Calls PlanetUse when a slot is clicked
	public void OnPointerDown(PointerEventData eventData) {

		//If selecting a planet, pause the game, only when orbiting sun
		if (gameController.isOrbiting())
		{
			pause.isPaused = true;
			PlanetUse ();
		}
	}

}
