﻿/* Created by Eric Poretsky */

/* Finds all planet's on screen and places them back into the inventory */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Reset : MonoBehaviour {

	public Inventory inventory;

	private bool reset;
	private GameObject[] allPlanets;
	private GameObject[] activePlanets;

	private Transform devPosition;

	/* Grabs all planets in the scene */
	void Start () {
		
		allPlanets = GameObject.FindGameObjectsWithTag ("Planet");
		reset = true;

	}
	
	/*Finds all planets that are active (on screen and not in the inventory), sets them to inactive,
	and adds them to the Inventory */
	void Update () {

		activePlanets = new GameObject[allPlanets.Length];

		int i = 0;

		/* Finds all planets that are on the screen (not in inventory) and adds them to the
		 active planet array */
		foreach (GameObject planet in allPlanets) {
			if (planet.activeInHierarchy) {
				activePlanets [i] = planet;
				i++;
			}
		}

		//if the user hits reset, get all the activePlanets and add them to the inventory
		if (reset == true) {
			
			foreach (GameObject planet in activePlanets) {

				//if the planet exists (is active), add it to the inventory and set it to false
				if (planet != null) {
					inventory.AddItem (planet.GetComponent<Item> ()); 
					planet.SetActive (false);
				}
			}
			reset = false;
		}
	}

	//Toggle the planet reset
	public void ResetPlanets() {
		reset = true;
	}
}
