﻿// Created by Rashid Epps

/*
	This script is used to control the asteroid objects
	within the game, it randomly generates one of the asteroid
	sprites, then it also adds a random torque to the asteroids
	to make them spin slowly.
*/

using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour {

	private Rigidbody2D rb;
	private GameObject explosionObject;

	private Sprite[] asteroidSprites;

	// Load all the sprites from resources folder upon awake.
	void Awake()
	{
		asteroidSprites = Resources.LoadAll<Sprite>("Asteroids");
	}

	// Add all of the necessary components for the asteroid
	void Start () {

		// Add the sprite renderer and set it to a random asteroid sprite
		gameObject.AddComponent<SpriteRenderer>();
		GetComponent<SpriteRenderer>().sprite = asteroidSprites[Random.Range(0, 4)];

		// Add the polygon collider
		gameObject.AddComponent<PolygonCollider2D>();
		GetComponent<PolygonCollider2D>().isTrigger = true;

		rb = GetComponent<Rigidbody2D>();

		// Add a random torque to the asteroid
		rb.AddTorque(Random.Range(-10.0f, 10.0f));



	}

}
