﻿// Created by Rashid Epps

/*
	This script is used to move the background back
	and forth within the game. The length variable refers 
	to how large the given background is and can be set publicly.
*/


using UnityEngine;
using System.Collections;

public class BackgroundPingPong : MonoBehaviour {

	private float leftBoundary;
	private float rightBoundary;

	public int length;
	
	// Update is called once per frame
	void Update () {
	
		// Ping Pong the background position.
		transform.position = new Vector3 (Mathf.PingPong(Time.time/8, length), 0.0f, 0.0f);

	}
}
