// Created by Rashid Epps

/* This script is used for the main camera to follow
 * Dev as he moves from left to right through the level
 * 
 * This script is also used to follow planets as they are being dragged
 *
 *
 *	farLeft and farRight are used as the margins for the
 *	camera boundaries.
 */

using UnityEngine;
using System.Collections;

public class ObjectFollow : MonoBehaviour {

	private GameObject camera;
	private Pause pause;
	private Transform farLeft;
	private Transform farRight;

	void Start() {

		// Setting the boundaries of our screen
		farLeft = GameObject.FindWithTag("Left").transform;
		farRight = GameObject.FindWithTag("Right").transform;

		//Find dev upon start
		camera = GameObject.FindWithTag("MainCamera");

		GameObject pauseObject = GameObject.FindWithTag("PauseScreen");
        if (pauseObject != null) 
        {
            pause = pauseObject.GetComponentInChildren <Pause>();
        }
	}

	public void OnMouseDrag()
	{
		if (pause.isPaused)
		{
			//Turn off the camera drag script
			camera.GetComponent<CameraDrag>().enabled = false;

			/* If the position of the object being dragged is greater than a certain
			*	bound, then we would like the camera to move left or right
			*	while constraining the boundaries
			*/

			if (transform.position.x > camera.transform.position.x + 4)
			{
				camera.transform.Translate(new Vector2(0.1f, 0), Space.World);
			}
			if (transform.position.x < camera.transform.position.x - 4)
			{
				camera.transform.Translate(new Vector2(-0.1f, 0), Space.World);
			}

			Vector3 newPosition = camera.transform.position;
			newPosition.x = Mathf.Clamp(newPosition.x, farLeft.position.x, farRight.position.x);
			camera.transform.position = newPosition;
		}

	}

	void OnMouseUp ()
	{
		//re-enable the camera drag script after dragging
		camera.GetComponent<CameraDrag>().enabled = true;

	}
}
