﻿// Created by Rashid Epps

/*
    This Game Controller is attached to an empty gameobject within
    the scenes. It controls a number of components for our game, such as 
    deciding if the player has won, the amound of deaths, and if the 
    player is at the starting position.
*/


using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    private GameObject eventSystem;
    private LevelManager levelManager;
    private int lastLevel;                          // holds the index of the last level played
    private Pause pause;
    private GameObject mainCamera;

    private AudioSource winSFX;

    public Text winText;
    private Text deaths;

    private bool win;
    private Collider2D sunCollider;
    private Vector3 devStartPosition;

    private int deathCounter = 0;

    void Start () 
    {
        // Ensure that the frame rate of our game is 60, on iOS devices
        Application.targetFrameRate = 60;

        // Grab the collider of the sun
        sunCollider = GameObject.FindWithTag("Sun").GetComponent<CircleCollider2D>();

        // Grab the event system
        eventSystem = GameObject.Find("EventSystem");

        // Winning the level sound affect
        winSFX = GetComponent<AudioSource>();

        // Get the starting position of the player
        devStartPosition = GameObject.FindWithTag("Player").transform.position;

        // Set win to false and the winning text to be empty
        win = false;
        winText.text = "";

        // Grab the GUiText component for the death counter
        deaths = GameObject.Find("Counter").GetComponent<Text>();

        //Find camera upon start
        mainCamera = GameObject.FindWithTag("MainCamera");

        GameObject pauseObject = GameObject.FindWithTag("PauseScreen");
        if (pauseObject != null) 
        {
            pause = pauseObject.GetComponentInChildren <Pause>();
        }

        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        //The last level that was played
        lastLevel = levelManager.lastLevel;

    }
	

	void Update () 
	{
        // If the game is paused then enable camera dragging
        if (pause.isPaused)
        {
            if (!GameObject.FindWithTag("MainCamera").GetComponent<CameraDrag>().enabled && !Input.GetMouseButton(0))
            {
                EnableCameraDrag();
            }
        }
	}

    public void EnableCameraDrag ()
    {
        mainCamera.GetComponent<CameraDrag>().enabled = true;
    }

    public void DisableCameraDrag ()
    {
        mainCamera.GetComponent<CameraDrag>().enabled = false;
    }

    public void WinLevel ()
    {
        winText.text = "Congratulations!";

        // Unlock the next level upon win
        levelManager.levelButtons[lastLevel + 1] = true;
        winSFX.Play();
        win = true;
    }

    public Vector3 GetStartPosition() 
    {
        return devStartPosition;
    }

    public void DisableTouch()
    {
        eventSystem.SetActive(false);
    }

    public void EnableTouch()
    {
        eventSystem.SetActive(true);
    }

    public bool isOrbiting()
    {
        return sunCollider.enabled;
    }

    public void UpdateDeath()
    {
        deathCounter += 1;
        deaths.text = "x" + deathCounter;
    }


}
