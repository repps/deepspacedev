﻿// Created by Rashid Epps

/*
	This script is used to control the comet objects by applying
	an initial force, and also setting the time it will respawn.
*/


using UnityEngine;
using System.Collections;

public class Comet : MonoBehaviour {

	public float cometRespawn;
	public float cometSpeed;

	private float nextComet;
	private Vector2 startPosition;
	private float startRotation;
	private GameObject comet;




	// Get the start position and rotation of the comet upon start
	void Start () {

		startPosition = transform.position;
		startRotation = transform.eulerAngles.z;
			
	}
	

	void Update () {

		// Check if the level time is greater than the respawn time, then load the next Comet
		if (Time.timeSinceLevelLoad > nextComet)
        {
            nextComet = Time.timeSinceLevelLoad + cometRespawn;

            if (transform.childCount != 0)
            {
                Destroy(transform.GetChild(0).gameObject);
            }
            comet = (GameObject) Instantiate(Resources.Load("Comet"), startPosition, Quaternion.Euler(0.0f, 0.0f, startRotation));
            comet.transform.parent = gameObject.transform;
            Rigidbody2D cometRB = comet.GetComponent<Rigidbody2D>();

            Vector2 force = comet.transform.up * cometSpeed;
            cometRB.AddForce(force, ForceMode2D.Impulse);

        }
	
	}
}
