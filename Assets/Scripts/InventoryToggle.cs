﻿/* Created by Eric Poretsky */

/* UI Button for Opening/Closing the Inventory */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InventoryToggle : MonoBehaviour {

	public bool show;

	//used to grab all the slot's once
	private bool once; 

	//all the slots
	private GameObject[] slots; 
	private GameObject[] planets;
	private Text planetCount;
	private int slotCount;

	private Image inventory;
	public Sprite many;
	public Sprite zero;

	void Start () {
		once = true;
		show = true;
		planetCount = GameObject.Find ("PlanetCount").GetComponent<Text> ();
		planets = GameObject.FindGameObjectsWithTag ("Planet");
		inventory = GameObject.Find ("InventoryToggle").GetComponent<Image> ();
	}

	/* Displays all the slots in the slot's array as well as the current number of planets
	in the inventory. */
	void Update () {

		if (once == true) {
			slots = GameObject.FindGameObjectsWithTag ("Slot");
			once = false;
		}

		foreach (GameObject slot in slots) {
			slot.SetActive (show);
		}
		slotCount = 0;
		foreach (GameObject planet in planets) {
			if (planet.activeInHierarchy) {
				slotCount++;
			}
		}

		planetCount.text = (planets.Length-slotCount).ToString(); 

		if (planets.Length - slotCount == 0) {
			inventory.sprite = zero;
		} else {
			inventory.sprite = many;
		}
	}

	/* Used for toggling the inventory on/off */
	public void Toggler () {
		show = !show;
	}
}
