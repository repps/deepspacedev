// Created by Rashid Epps

/*
	This script is used to keep track of which levels have been played,
	and also which levels have been beaten so that the next level can be unlocked.
	This is attached to an empty game object and not destroyed throughout our game.
*/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LevelManager : MonoBehaviour {

	public int lastLevel;						//the variable that holds the index of the last level
	private GameObject levels;					
	public static LevelManager instance;		//Only hold one instance of the level Manager

	public Dictionary<int, bool> levelButtons = new Dictionary<int, bool>(); 		//Dictionary to look up the levels and their buttons

	private Button[] buttons;
	private bool grabbedLevels = false;
	private float waitTime = 6.0f;				//The waiting time to unlock all levels
	private float pressTime;					//Variable to hold how long you press

	// Use this for initialization
	void Start () {

		// Do not destroy this game object
		// Ensure that there is always one instance of this object
		DontDestroyOnLoad(gameObject);

		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			GameObject.Destroy(gameObject);
		}

		Object.DontDestroyOnLoad(gameObject);
	
	}
	
	// Update is called once per frame
	void Update () {

		// If the screen is pressed longer than 6 seconds, all levels will be unlocked.
		if (Input.GetMouseButton(0))
		{
			pressTime += Time.deltaTime;
			
			if (pressTime > waitTime)
			{
				buttons = levels.GetComponentsInChildren<Button>();

				foreach (Button button in buttons)
				{
					button.interactable = true;
				}

			}

		}
		
		//Reset the pressTime when released
		if (Input.GetMouseButtonUp(0))
		{
			pressTime = 0.0f;
		}
	
	}

	void OnLevelWasLoaded(int level)
	{
		// If the scene is NOT the NextlevelScene, then we want to save the index of the current level
		if (level != 2)
		{
			lastLevel = level;
		}

		// Get all the levels at the beginning of the game and set them to locked
		if (level == 1 && !grabbedLevels)
		{
			levels = GameObject.Find("Levels");
			buttons = levels.GetComponentsInChildren<Button>();
 
			foreach (Button button in buttons)
			{
				int levelIndex = button.transform.GetSiblingIndex() + 6;
				button.interactable = false;
				levelButtons.Add(levelIndex, button.interactable);
			}
			grabbedLevels = true;
		}

		// Reset the level unlock or locked parameter accordingly on level select load
		if (level == 1)
		{
			levels = GameObject.Find("Levels");
			buttons = levels.GetComponentsInChildren<Button>();
			foreach(KeyValuePair<int, bool> entry in levelButtons)
			{
				buttons[entry.Key - 6].interactable = entry.Value;
			}
		}

	}
}
