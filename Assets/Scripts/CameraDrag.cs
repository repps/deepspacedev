﻿// Created by Rashid Epps 

/*
    Attach this script to the camera to allow the player to drag
    and move the position left and right so that they can view the
    entire level.
*/

using UnityEngine;
using UnityEngine.EventSystems;
 
public class CameraDrag : MonoBehaviour
{
    public float dragSpeed = 2;                             //The speed to which the camera moves
    public Transform farLeft;
    public Transform farRight;
    private Vector3 dragOrigin;

    private Pause pause;                                    //Pause object

    void Start () 
    {
        // Grab the pause componnet of the game
        GameObject pauseObject = GameObject.FindWithTag("PauseScreen");
        if (pauseObject != null) 
        {
            pause = pauseObject.GetComponentInChildren <Pause>();
        }
        else
        {
            Debug.Log("Can't find pause object");
        }

    }
 
 
    void Update()
    {
        // Only move the camera when the game is paused and mouse is not over a game object
        if (!EventSystem.current.IsPointerOverGameObject()) {
            if (pause.isPaused)                                 
            {
                if (Input.GetMouseButtonDown(0))
                {
                    dragOrigin = Input.mousePosition;
                    return;
                }
         
                if (!Input.GetMouseButton(0)) return;
         
                Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
                Vector3 move = new Vector3(pos.x * dragSpeed, 0, 0);

                Vector3 newPosition = transform.position;
                newPosition.x = Mathf.Clamp(newPosition.x, farLeft.position.x, farRight.position.x);
                transform.position = newPosition;
         
                transform.Translate(move, Space.World);  
            }
        }
    }
 
 
}
