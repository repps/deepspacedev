﻿// Created by Rashid Epps

//	*********CURRENTLY NOT BEING USED********

/*
	This script is used to position the second tail of our comet towards
	the sun, to simulate the second tail of a comet in real space.
*/
	


using UnityEngine;
using System.Collections;

public class CometTail : MonoBehaviour {

	private GameObject sun;
	private float angle;

	// Use this for initialization
	void Start () {

		sun = GameObject.FindWithTag("Sun");
	
	}
	
	// Update is called once per frame
	void Update () {

		angle = Vector3.Angle(sun.transform.position, transform.position);
		transform.localEulerAngles = new Vector3 (-(angle - 90.0f), 90.0f, 0.0f);
	
	}
}
