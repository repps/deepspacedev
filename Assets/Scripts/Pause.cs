﻿/* Created by Eric Poretsky */

/* UI Button for pausing and unpausing the game*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pause : MonoBehaviour {

	private GameController gameController;
	private Renderer devR;

	public bool isPaused;

	private Button pauseButton;
	private Image pausePlay;
	private Sprite pause;
	public Sprite play;

	void Start () {
		isPaused = false;
		pausePlay = GameObject.Find ("Pause/Play").GetComponent<Image>();
		pause = pausePlay.sprite;
		pauseButton = GameObject.Find ("Pause/Play").GetComponent<Button> ();
	}
	
	// Checks to see if the pauseButton is on and changes the game's time scale on press
	// Also changes the pause sprite to play if the game is paused and vice-versa
	void Update () {

		if (pauseButton.interactable) {
			if (isPaused == true) {
				Time.timeScale = 0;
				pausePlay.sprite = play;

			} else if (isPaused == false) {
				Time.timeScale = 1;
				pausePlay.sprite = pause;
			}
		}
		else if (isPaused == true)
		{
			Time.timeScale = 0;
			pausePlay.sprite = play;
		}
	}

	/* Toggles the pause on and off */
	public void PauseGame() {

		isPaused = !isPaused;
	}
}
