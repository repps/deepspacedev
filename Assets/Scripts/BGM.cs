﻿// Created by Rashid Epps

/*
	This script is used to ensure that there is always
	exacty one background music object within each scene 
	of our game.
*/


using UnityEngine;
using System.Collections;

public class BGM : MonoBehaviour {

	public static BGM instance;

	// Use this for initialization
	void Start () {

		// This implements the "singleton" property that we want

		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			GameObject.Destroy(gameObject);
		}

		Object.DontDestroyOnLoad(gameObject);
	
	}
	
}
