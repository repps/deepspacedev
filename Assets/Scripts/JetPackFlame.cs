﻿// Created by Rashid Epps

/*
	This script is used to control the jet pack flames of the player,
	when tapping the screen or pressing space bar, the jet pack flames
	are set to play.
*/


using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class JetPackFlame : MonoBehaviour {

	private Renderer devRenderer;
	private ParticleSystem jetpack;

	private string colliderName;
	// Use this for initialization

	void Awake () {

		devRenderer = GetComponent<Renderer>();

		// Grab the particle system component and set it to stop
		jetpack = GetComponent<ParticleSystem> ();
		jetpack.Stop ();
		jetpack.Clear ();
	}

	void FixedUpdate () {

		// If tapping the screen or pressed space, then the flames are played
		if (Input.GetKeyDown("space") || Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
		{
			//Play jetpack animation only if object not selected
			Vector2 worldPoint = Camera.main.ScreenToWorldPoint( Input.mousePosition );
	        RaycastHit2D hit = Physics2D.Raycast( worldPoint, Vector2.zero );
	        if (hit.transform != null)
	        {
	        	colliderName = hit.collider.transform.name;
	        }
	        if (colliderName != "Surface")
	        {
	        	jetpack.Play ();
	        }
		}
	}


}
