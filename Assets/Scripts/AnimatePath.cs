﻿// Created by Rashid Epps

/*
	This script is used to animate the dots of the trajectory 
	path, it turns the dots on and off in succession
	to convey forward direction and trajectory.
*/


using UnityEngine;
using System.Collections;

public class AnimatePath : MonoBehaviour {

	private Transform[] dots;
	private float speed = 0.1f;
	private float nextDot;
	private int i = 1;
	private int j = 8;

	private bool drawn = false;

	// Use this for initialization
	void Start () {

		dots = GetComponentsInChildren<Transform>();
	
	}
	
	// Update is called once per frame
	void Update () {

		//Only run this code once if dots are not instantiated
		if (dots.Length == 1 && !drawn)
		{
			drawn = true;
			dots = GetComponentsInChildren<Transform>();

		} 

		if (Time.timeSinceLevelLoad > nextDot)
		{
			if (i > 0)
			{
				dots[i-1].gameObject.SetActive(true);
				dots[j-1].gameObject.SetActive(true);
			}
			nextDot = Time.timeSinceLevelLoad + speed;
			dots[i].gameObject.SetActive(!dots[i].gameObject.activeSelf);
			dots[j].gameObject.SetActive(!dots[j].gameObject.activeSelf);
			i++;
			j++;

			if (i == dots.Length - 1)
			{
				i = 1;
			}

			if (j == dots.Length - 1)
			{
				j = 1;
			}

		}

	
	}
}
