﻿// Created by Rashid Epps

/*
	This script is attached to our worm hole objects and teleports
	the player to the other worm hole that is attached to this script
	via the public variable.
*/


using UnityEngine;
using System.Collections;

public class WarpHole : MonoBehaviour {

	public float warpSpeed;							// The speed that the player gets sucked into by the worm hole
	public Transform warpHole2;						// The transform of the second worm hole

	private Collider2D core;
	private Transform areaOfEffect;
	private AudioSource wormHoleSFX;

	private Vector2 enterVelocity;					// The velocity at which the player enters the worm hole
	private Quaternion enterRotation;				// The rotation at which the player enters the worm hole
	private Vector3 enterScale;						// The scale at which the player enters the worm hole

	private bool entered = false;


	// Use this for initialization
	void Start () {

		//initialize the second warpHole and other components
		areaOfEffect = this.transform.FindChild("Area of Effect");
		CircleCollider2D collider = GetComponent<CircleCollider2D>();
		core = this.transform.FindChild("Core").gameObject.GetComponent<CircleCollider2D>();

		wormHoleSFX = GetComponent<AudioSource>();


		//scale the area of effect with the collider
		areaOfEffect.localScale = areaOfEffect.localScale * collider.radius * 2;


	
	}
	

	void OnTriggerEnter2D(Collider2D other)
	{
		if (!entered && other.tag == "Player")
		{
			//Save the incoming velocity
			entered = true;
			enterVelocity = other.attachedRigidbody.velocity;
			enterRotation = other.transform.rotation;
			enterScale = other.transform.localScale;

		}

	}

	void OnTriggerStay2D (Collider2D other)
	{
		// if object is touching the core, then shrink and teleport to other warp
		if (core.IsTouching(other))
		{
			other.transform.localScale -= new Vector3(0.0f, Time.deltaTime, 0.0f);

			if (other.transform.localScale.y < 0)
			{
				other.gameObject.transform.position = warpHole2.position;


			}

		}

		// Stretch the object during warp
		if (other.tag == "Player")
		{
			// As dev enters the worm hole, have them quickly spaghetiffy
			other.attachedRigidbody.velocity = other.attachedRigidbody.velocity/100;

			float step = Time.deltaTime * warpSpeed;
			other.transform.position = Vector3.MoveTowards(other.transform.position, core.transform.position, step);
			other.transform.localScale -= new Vector3(Time.deltaTime * 0.08f, 0.0f, 0.0f);
			other.transform.localScale += new Vector3(0.0f, Time.deltaTime * 0.08f, 0.0f);


		}
	}

	// When leaving the first warp reset the scale and velocity of object
	void OnTriggerExit2D (Collider2D other)
	{
		if (other.tag == "Player")
		{
			wormHoleSFX.Play();
			other.attachedRigidbody.velocity = enterVelocity;
			other.transform.rotation = enterRotation;
			other.transform.localScale = enterScale;
			entered = false;
		}
	}
}
