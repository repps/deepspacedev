﻿// Created by Rashid Epps

/*
	This small script simply allows for a gameobject to exist between loading
	different scenes.
*/


using UnityEngine;
using System.Collections;

public class DontDestroy : MonoBehaviour {

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(gameObject);
	}
	
}
