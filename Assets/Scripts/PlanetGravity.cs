﻿// Created by Rashid Epps

/*
	This script is used to control the planets and all of 
	their properties/features. It controls how the moon orbits 
	the planet, the gravitational force of the planet, and how 
	fast the player rotates around the planet
*/


using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;


public class PlanetGravity : MonoBehaviour 
{
	private float G;

	public float rotationSpeed;				//how fast dev rotates within the field
	public float range;						//range of the planet's gravity
	public float moonSpeed;					//speed of the orbiting moon
	public bool clockwise;					//Direction of the moon

	private Rigidbody2D rb;
	private Rigidbody2D dev;
	private Rigidbody2D moon;

	private Vector2 relativePos;
	private float angle;
	private Quaternion rotate;
	private GameObject explosionObject;

	private GameObject[] jetPackIcons;
	private AudioSource crash;
	//This object is just used to visually show the player when they will be pulled
	private GameObject areaOfEffect;

	void Awake() 
	{
		// Grab the jet pack icons on the screen
		jetPackIcons = GameObject.FindGameObjectsWithTag ("JetIcon");
		
		//Setting all of the variables upon awake
		G = 0.4f;
		dev = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D> ();
		areaOfEffect = this.transform.FindChild("Area of Effect").gameObject;
		rb = GetComponent<Rigidbody2D>();
		CircleCollider2D collider = GetComponent<CircleCollider2D>();
		range = collider.radius * transform.localScale.x;

		Renderer aoeColor = areaOfEffect.GetComponent<Renderer>();
		
		// Change the color of the gravity field depending on the planet mass
		if (rb.mass >= 100)
		{
			aoeColor.material.SetColor ("_Color", Color.yellow);
		}
		if (rb.mass >= 50 && rb.mass < 100)
		{
			aoeColor.material.SetColor ("_Color", Color.blue);
		}


		//Calculates the appropriate scale for the AoE circle so it matches the radius of the collider
		//float scale = (collider.radius * 0.3791469194f);
		float scale = (collider.radius * 2);
		areaOfEffect.transform.localScale = new Vector3 (scale, scale, scale);

	}

	void OnEnable ()
	{
		//Make the planet spin slowly by adding torque
		rb.AddTorque(4.0f, ForceMode2D.Force);
	}

	//Calculates the attraction force between the planet and Dev
	Vector2 Attract()
	{
		//Get the distance between dev and the planet
		Vector2 force = (rb.position - dev.position);
		float distance = force.magnitude;

		//distance = Mathf.Clamp(distance, 10.0f, 100.0f);

		//Change force to a unit vector and calculate the strength using the gravity formula
		force.Normalize();
		float strength = ((G * rb.mass * dev.mass) / (distance * distance));
		force *= strength;

		return force;
	}

	//if the player flies offscreen this planet needs a reference to dev's rigidbody still!
	void Update() {
		if (dev == null) {
			dev = GameObject.FindWithTag ("Player").GetComponent<Rigidbody2D> ();
		}


		//Destroy the Explosion object if it is done playing
		if (explosionObject != null && !explosionObject.GetComponent<ParticleSystem>().isPlaying)
		{
			GameObject.Destroy (explosionObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		//Calculate the vector that points from Dev to planet
		//If dev is facing the planet, rotate forwards, if not, backwards.
		if (IsFacing())
		{
			relativePos = (dev.position - rb.position);
		}
		else
		{
			relativePos = (rb.position - dev.position);
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{

		//Else have the planet's gravity attract dev
		if (other.tag == "Player")
		{			
			//Calculates the attraction force
			Vector2 force = Attract();

			//Based on the relative vector, this angle makes sure that Dev is perpendicular to the vector
			angle = Mathf.Atan2(relativePos.y, relativePos.x) * Mathf.Rad2Deg;

			//The Direction in which dev rotates depends on which side of the planet hes on.
			rotate = Quaternion.AngleAxis(Mathf.Round(angle), transform.forward);

			//Rotates Dev towards the planet, then adds the attraction force
			//dev.transform.localRotation = Quaternion.RotateTowards(current, rotate, rotationSpeed);
			dev.transform.localRotation = Quaternion.RotateTowards(dev.transform.localRotation, rotate, rotationSpeed);

			dev.AddForce(force, ForceMode2D.Force);
		}

		if (other.tag == "Moon") 
		{

			Vector3 orbitDir;
			moon = other.GetComponent<Rigidbody2D>();

			//Calculate the vector that points from Moon to planet
			Vector2 relativePos = (moon.position - rb.position);

			//Based on the relative vector, this angle makes sure that Dev is perpendicular to the vector
			float angle = Mathf.Atan2(relativePos.y, relativePos.x) * Mathf.Rad2Deg;

			//This float is added to the angle to prevent dev from spiraling outwards
			float offset = moonSpeed/2 + 0.1f;

			//This is the direction to which Dev should rotate towards
			Quaternion rotate = Quaternion.AngleAxis(Mathf.Round(angle) + offset, Vector3.forward);

			Quaternion current = moon.transform.rotation;

			//Changes the orbit direction of the moon
			if (clockwise)
			{
				orbitDir = Vector3.down;
			}
			else
			{
				orbitDir = Vector3.up;
			}

				//Rotates moon, and moves the position clockwise or Counter clockwise relative to itself
				moon.transform.rotation = Quaternion.RotateTowards(current, rotate, Time.deltaTime * 50000);
				moon.transform.Translate(orbitDir * Time.deltaTime * moonSpeed);
		}
	}

	bool IsFacing()
	{
		// Use the dot product to tell whether dev is facing the planet or not
		float dot = Vector3.Dot(dev.transform.right, (rb.position - dev.position).normalized);
		if (dot < 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}