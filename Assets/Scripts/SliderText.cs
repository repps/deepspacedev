﻿// Created by Rashid Epps

/*
	This script simply displays the value of the speed slider within 
	the menu panel of the game so that the player can see how much
	they are increasing/decreasing the speed of dev orbiting the sun.
*/

 using UnityEngine;
 using UnityEngine.UI;
 
 public class SliderText : MonoBehaviour 
 {
     string sliderTextString = "0";
     public Text sliderText; 							// public is needed to ensure it's available to be assigned in the inspector.
 
     public void textUpdate (float textUpdateNumber)
     {
         sliderTextString = textUpdateNumber.ToString();
         sliderText.text = sliderTextString;
     }
 }
