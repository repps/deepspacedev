﻿// Created by Rashid Epps

/*
	This script is used to make the backgrounds of our game 
	scroll slowly in the distance, the tileSize should be equal to 
	the width of the background, and the scroll speed can be publicly
	set
*/


using UnityEngine;
using System.Collections;

public class BGScroll : MonoBehaviour
{
	public float scrollSpeed;
	public float tileSizeZ;

	private Vector3 startPosition;

	void Start ()
	{
		// Get the start position upon awake
		startPosition = transform.position;
	}

	void Update ()
	{
		// Scroll through the background objects and repeat the position of the background
		float newPosition = Mathf.Repeat(Time.timeSinceLevelLoad * scrollSpeed, tileSizeZ);
		transform.position = startPosition + Vector3.right * newPosition;
	}
}