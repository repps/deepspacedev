﻿// Created by Eric Poretsky

/*
	This script is just used to control the toggles in our menu screen.
	It grabs all of the planet colliders within the scene, and sets them
	according to the toggle.
*/



using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DotsToggle : MonoBehaviour {

	private Toggle gravityToggle;

	private GameObject[] areasOfEffect;

	// Use this for initialization

	void Start () {
		gravityToggle = GameObject.Find ("Gravity").GetComponent<Toggle> ();
		areasOfEffect = GameObject.FindGameObjectsWithTag ("AoE");
	}
	
	// Update is called once per frame
	void Update () {
		//add code for making sure if dev dies his toggle stays

		// Set the toggle for all of the AoE objects
		foreach (GameObject area in areasOfEffect) {
			area.SetActive (gravityToggle.isOn);
		}
	}
}
