﻿// Created by Rashid Epps


// **************CURRENTLY NOT USED*************

/*
	This script determines if the current device is an iOS
	device, and uses the correct method to play the movie using 
	the device's media player.
*/


using UnityEngine;
using System.Collections;

public class PlayVideo : MonoBehaviour {

	// Use this for initialization
	void Start () {
		#if UNITY_IOS
			Handheld.PlayFullScreenMovie("InstructionTrailer.mp4");
		#endif
	}

	void Update ()
	{
		if (Input.GetMouseButtonDown(0))
		{
			Application.LoadLevel("LevelSelect");	
		}
	}
}
