﻿// Created by Rashid Epps

/*
	This script is used to animate the gravitational
	field around our planets, the speed of the animation
	can be freely changed.
*/

using UnityEngine;
 
public class AnimateField : MonoBehaviour 
{
	public GameObject iShield;
	public float iSpeed;
 
	private Material mMaterial;
	private float mTime;
 
	// Grab the material of our Game object
	void Start () 
	{
		mMaterial = iShield.GetComponent<Renderer>().material;
		 
		mTime = 0.0f;
	}
 
	// Update the offset of the material with deltaTime and our speed
	void Update () 
	{
		mTime += Time.deltaTime * iSpeed;
 
		mMaterial.SetFloat ("_Offset", Mathf.Repeat (mTime, 1.0f));

	}
}