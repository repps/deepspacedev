﻿// Created by Rashid Epps

/*
	This script controls the black hole objects within our game
	attach this to a black hole to give it sound effects and 
	proper animation.
*/


using UnityEngine;
using System.Collections;

public class BlackHole : MonoBehaviour {

	public float deathSpeed;

	private GameController gameController;


	private Rigidbody2D dev;
	private Collider2D core;
	private GameObject[] jetPackIcons;
	private Transform areaOfEffect;
	private AudioSource blackHoleSFX;
	// Use this for initialization
	void Start () {

		//Assign gameController
		gameController = GameObject.FindWithTag("GameController").GetComponent<GameController>();

		//Initialize dev and the core of the black hole
		dev = GameObject.FindWithTag("Player").GetComponent<Rigidbody2D> ();
		core = this.transform.FindChild("Core").gameObject.GetComponent<CircleCollider2D>();

		// Grab the jetPackIcons to reset when dev dies
		jetPackIcons = GameObject.FindGameObjectsWithTag ("JetIcon");
		areaOfEffect = this.transform.FindChild("Area of Effect");
		CircleCollider2D collider = GetComponent<CircleCollider2D>();
		blackHoleSFX = GetComponent<AudioSource>();

		// Scale the area of effect to equal the collider of the black Hole
		areaOfEffect.localScale = areaOfEffect.localScale * collider.radius * 2;

	
	}
	
	// Update is called once per frame
	void Update () {
		//Obtain dev if he dies
		if (dev == null) 
		{
			dev = GameObject.FindWithTag ("Player").GetComponent<Rigidbody2D> ();
		}
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		// Play the black hole soundFX upon entering
		if (other.tag == "Player")
		{
			blackHoleSFX.Play();
		}
	}

	void OnTriggerStay2D(Collider2D other)
	{
		// If the player reaches the core of the object, shrink their scale
		if (core.IsTouching(other))
		{
			other.transform.localScale -= new Vector3(0.0f, Time.deltaTime, 0.0f);
			other.attachedRigidbody.velocity = Vector2.zero;

			if (other.transform.localScale.y < 0)
			{
				//Destroy dev and update counter
				GameObject.Destroy (GameObject.FindGameObjectWithTag ("Player"));
				gameController.UpdateDeath();

				//Re enable the sun collider
				CircleCollider2D sunCol = GameObject.FindWithTag ("Sun").GetComponent<CircleCollider2D> ();
				sunCol.enabled = true;

				//Re-Loading Dev from the resource folder 
				GameObject devC = (GameObject)Instantiate(Resources.Load("Dev"), gameController.GetStartPosition(), Quaternion.identity);
				dev = devC.GetComponent<Rigidbody2D>();

				foreach (GameObject jet in jetPackIcons) {
						jet.SetActive (true);
				}
			}

		}
		
		if (other.tag == "Player")
		{
			//As dev enters the black hole, have them slowly spaghetiffy by increasing the sprite scale
			other.attachedRigidbody.velocity = other.attachedRigidbody.velocity/100;

			other.transform.FindChild("JetPack Flames").gameObject.SetActive(false);

			float step = Time.deltaTime;
			other.transform.position = Vector3.MoveTowards(other.transform.position, core.transform.position, step);
			other.transform.localScale -= new Vector3(Time.deltaTime * deathSpeed, 0.0f, 0.0f);
			other.transform.localScale += new Vector3(0.0f, Time.deltaTime * deathSpeed, 0.0f);


		}
	}
}
