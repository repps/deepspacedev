﻿// Created by Rashid Epps

/*
    This script manages the scenes and their transitions within
    the game and the loading screen. Most of the functions within
    this script are simply attached to the buttons in our level select
    scene, in order to know which level to play next
*/


using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SceneManagerScript : MonoBehaviour {

	private bool loadScene = false;
	private bool clickedLevel = false;                             // boolean to tell when the user has click a level
    private bool clickPlay = false;                                // whether the player has clicked the play button
    private bool animation = false;                                // whether the animation in the loading screen has played
    private GameObject loadingScreen;
    private LevelManager levelManager;
    private int lastLevel;
    private Animator shipAnimator;

    private GameObject background;
    private GameObject ui;

    [SerializeField]
    public string levelToLoad;
    [SerializeField]
    private Text loadingText;

	// Use this for initialization
	void Start () {
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        shipAnimator = GameObject.Find("DevShip").GetComponent<Animator>();

        // Hold the index of the last scene that was played
        lastLevel = levelManager.lastLevel;

        // Reset the time scale to ensure it is 1
        Time.timeScale = 1;

        // Find the loading screen and set it to inactive during our level select screen
        loadingScreen = GameObject.Find("LoadingScreen");
        if (loadingScreen != null)
        {
            if (Application.loadedLevel != 2)
            {
                loadingScreen.SetActive(false);
            }
        }

        // The background and UI components of the level select screen
        background = GameObject.Find("Background");
        ui = GameObject.Find("UI");

	}
	

    // Updates once per frame
    void Update () {

        // If the user has clicked a level, and the dev ship animation has not played yet
        // Play the animation, and destroy the background and UI of the level select screen
        // Set the UI of the loading screen to be active
        if (clickedLevel && animation == false)
        {            
            animation = true;


            GameObject.Destroy(ui);
            GameObject.Destroy(background);

            loadingScreen.SetActive(true);

            clickPlay = true;
            shipAnimator.Play("DevShipAlert");

        }

        // If the player has pressed the space bar and a new scene is not loading yet...
        if (clickPlay && loadScene == false) {

            // ...set the loadScene boolean to true to prevent loading a new scene more than once...
            loadScene = true;

            // ...change the instruction text to read "Loading..."
            loadingText.text = "Loading...";

            // ...and start a coroutine that will load the desired scene.
            StartCoroutine(LoadNewScene());

        }

        // If the new scene has started loading...
        if (loadScene == true) {

            // ...then pulse the transparency of the loading text to let the player know that the computer is still working.
            loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));

        }

    }

	public void LevelSelect()
	{
		Application.LoadLevel("LevelSelect");
	}

	public void TutorialOne()
	{
		clickedLevel = true;
		levelToLoad = "TutorialOne";
	}

    public void TutorialTwo ()
    {
        clickedLevel = true;
        levelToLoad = "TutorialTwo";
    }

    public void TutorialThree ()
    {
        clickedLevel = true;
        levelToLoad = "TutorialThree";
    }

    public void Comet ()
    {
        clickedLevel = true;
        levelToLoad = "Comet";
    }

    public void ShipClosed ()
    {
        clickedLevel = true;
        levelToLoad = "ShipClosed";
    }

    public void WormHole ()
    {
        clickedLevel = true;
        levelToLoad = "WormHole";
    }

    public void Portals()
    {
        clickedLevel = true;
        levelToLoad = "Portals";
    }

    public void ThreeComets()
    {
        clickedLevel = true;
        levelToLoad = "ThreeComets";
    }

    public void Gaps()
    {
        clickedLevel = true;
        levelToLoad = "Gaps";
    }

    public void OneBelt()
    {
        clickedLevel = true;
        levelToLoad = "OneBelt";
    }

    public void TwoBelts()
    {
        clickedLevel = true;
        levelToLoad = "TwoBelts";
    }

    public void CrissCross()
    {
        clickedLevel = true;
        levelToLoad = "CrissCross";
    }

    public void BlackHoles()
    {
        clickedLevel = true;
        levelToLoad = "BlackHoles";
    }

    public void BigHole()
    {
        clickedLevel = true;
        levelToLoad = "BigHole";
    }

	public void WorldOne()
	{
        clickedLevel = true;
		levelToLoad = "JetPacks";
	}

    public void TitleScreen()
    {
        Application.LoadLevel("TitleScreen");
    }

    public void ClickPlay ()
    {
        clickPlay = true;
    }

    public void NextLevel ()
    {
        clickPlay = true;
    }

    public void ReplayLevel ()
    {
        clickPlay = true;
        lastLevel -= 1;
    }

    public void PlayInstructions()
    {
        #if UNITY_IOS
            Handheld.PlayFullScreenMovie("InstructionTrailer.mp4");
        #endif
    }

    public void PlayCutscene()
    {
        #if UNITY_IOS
            Handheld.PlayFullScreenMovie("Cutscene.mov");
        #endif
    }

    public void PlayCredits()
    {
        Application.LoadLevel("TheEnd");
    }

	 // The coroutine runs on its own at the same time as Update() and takes an integer indicating which scene to load.
    public IEnumerator LoadNewScene() 
    {
        // This line waits for 4 seconds before executing the next line in the coroutine.
        yield return new WaitForSeconds(4);

        AsyncOperation async = null;

        // If the current level is not the level select screen, then load the clicked level
        // Else, load the next level
        if (Application.loadedLevel != 2)
        {
            // Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
            async = Application.LoadLevelAsync(levelToLoad);
        }
        else 
        {
            async = Application.LoadLevelAsync(lastLevel + 1);
        }

        // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
        while (!async.isDone) {
            yield return null;
        }

    }

}
