﻿// Created by Rashid Epps

/* This script is used for the main camera to follow
 * Dev as he moves from left to right through the level
 *
 *
 *	farLeft and farRight are used as the margins for the
 *	camera boundaries.
 */

using UnityEngine;
using System.Collections;

public class DevFollow : MonoBehaviour {

	private Transform dev;
	private Pause pause;
	public Transform farLeft;
	public Transform farRight;

	void Start() {

		//Find dev upon start
		dev = GameObject.FindWithTag("Player").transform;


		// Find the pause object upon start
		GameObject pauseObject = GameObject.FindWithTag("PauseScreen");
        if (pauseObject != null) 
        {
            pause = pauseObject.GetComponentInChildren <Pause>();
        }
	}

	void Update()
	{
		if (!pause.isPaused)
		{

			//If dev is reset then find him again
			if (dev == null)
			{
				dev = GameObject.FindWithTag("Player").transform;
			}

			/* Grab the transform position, then set it to devs x axis position while clamping
			*	the position between far left and right, and reset the transform position.
			*/
			Vector3 newPosition = transform.position;
			newPosition.x = dev.position.x;
			newPosition.x = Mathf.Clamp(newPosition.x, farLeft.position.x, farRight.position.x);
			transform.position = newPosition;
		}

	}
}
