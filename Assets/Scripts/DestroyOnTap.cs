﻿// Created by Rashid Epps

/*
	This script is used to destroy the tap animation when the 
	user has interacted with the screen. It also destroys the object
	after a certain amount of time has passed.
*/


using UnityEngine;
using System.Collections;

public class DestroyOnTap : MonoBehaviour {

	public float destroyTime;
	
	// Update is called once per frame
	void Update () {

		// Check if enough time has passed or if the player has interacted with the screen
		if (Time.timeSinceLevelLoad > destroyTime)
		{
			GameObject.Destroy(gameObject);
		}

		if (Input.GetMouseButtonDown(0))
		{
			GameObject.Destroy(gameObject);
		}
	
	}
}
