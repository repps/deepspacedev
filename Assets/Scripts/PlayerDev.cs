﻿// Created by Rashid Epps

/*
	This script is used to control the player and their animations
	and sounds. This script also controls the player jet packing
	on tap or press of the space bar.
*/


using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PlayerDev : MonoBehaviour 
{
	private Pause pause;
	private Rigidbody2D rb;
	private float nextJetPackFire;
	private Animator animator;
	private Renderer devRenderer;
	private PolygonCollider2D devCollider;

	private bool jetPacking = false;						// boolean to tell whether the player is in the jetpack animation
	private bool transition = false;						// boolean to tell whether the player is in the transition animation

	public int jetPackAccel;								// How fast the player will accelerate upon jet pack
	public float jetPackRate;								// How often the player is allowed to jetpack
	public AudioClip zeroJetPacks;

	private int jetCount; // change to prviate

	private GameObject[] jetIcons = new GameObject[3];
	private int jetIconCount;
	private AudioSource jetpackSound;

	private string colliderName;							//name of colider to check against Raycast


	void Start()
	{
		// Grab the player's collider
		devCollider = GetComponent<PolygonCollider2D>();

		// Grab the pause object
		pause = GameObject.FindWithTag("PauseScreen").GetComponent<Pause>();

		// Grab the player's renderer
		devRenderer = GetComponent<Renderer>();

		jetpackSound = GetComponent<AudioSource>();

		//Get Player animmator and rigidbody components
		animator = GetComponent<Animator>();
		rb = GetComponent<Rigidbody2D>();

		// Set the number of jetpacks to 3
		jetCount = 3;

		// Grab the jet pack icons in order
		jetIcons[0] = GameObject.Find("Jet1");
		jetIcons[1] = GameObject.Find("Jet2");
		jetIcons[2] = GameObject.Find("Jet3");
		jetIconCount = 0;
	}

		
	void Update()
	{
		//Check if dev is transitioning then update collider
		if (animator.GetCurrentAnimatorStateInfo(0).IsTag("Transition") && !transition)
		{
			transition = true;
			gameObject.AddComponent<PolygonCollider2D>();
			Destroy(GetComponent<PolygonCollider2D>());
		}

		//Reset the polygon collider so that it is more accurate upon animation
		if (animator.GetCurrentAnimatorStateInfo(0).IsTag("JetPack") && !jetPacking)
		{
			jetPacking = true;
			gameObject.AddComponent<PolygonCollider2D>();
			Destroy(GetComponent<PolygonCollider2D>());

		}

		//If space is pressed or click and game is not paused.
		if (Input.GetKeyDown("space") || Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject() && Time.time > nextJetPackFire && devRenderer.isVisible) 
		{
			/* Create a raycast to check whether the player has tapped on the surface
			 * of a planet or not
			 */
			Vector2 worldPoint = Camera.main.ScreenToWorldPoint( Input.mousePosition );
	        RaycastHit2D hit = Physics2D.Raycast( worldPoint, Vector2.zero );
	        if (hit.transform != null)
	        {
	        	colliderName = hit.collider.tag;
	        }
	        else
	        {
	        	colliderName = "Background";
	        }
	        
	        // Jetpack only if you are not touching a planet or another game object
			if (!pause.isPaused && colliderName != "Surface")
			{
				animator.SetTrigger("devJetPack");

				nextJetPackFire = Time.time + jetPackRate;

				// Remove the sun collider when dev takes off
				GameObject sun = GameObject.FindWithTag ("Sun");
				CircleCollider2D sColl = sun.GetComponent<CircleCollider2D> ();
				sColl.enabled = false;

				// Update the count when space is pressed
				if (jetCount > 0) {
					jetpackSound.Play();
					rb.AddRelativeForce(new Vector2(1, jetPackAccel));
					jetCount--;

					jetIcons [jetIconCount].SetActive (false);
					jetIconCount++;
				}
				if (jetCount < 0)
				{
					jetpackSound.PlayOneShot(zeroJetPacks, 0.3f);
				}
			}

		}
	}


}
