﻿// Created by Rashid Epps

/*
	This is a small script that is used to make dev blast away
	during our loading screen.
*/


using UnityEngine;
using System.Collections;

public class DevFlyAway : MonoBehaviour {

	private Rigidbody2D rb;

	// Use this for initialization
	void Start () {

		// Apply a force to dev as soon as this script is active
		rb = GetComponent<Rigidbody2D>();
		rb.AddForce(new Vector2 (400.0f, 0.0f), ForceMode2D.Impulse);
		rb.AddTorque(200.0f, ForceMode2D.Force);

	
	}
	
	// Update is called once per frame
	void Update () {

	}
}
