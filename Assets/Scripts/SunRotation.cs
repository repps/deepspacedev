﻿// Created by Rashid Epps

/*
	This script is attached to the sun and as long as the player is within
	the collider, the player is rotated around the sun at a certain speed.
*/


using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SunRotation : MonoBehaviour
{

	private Rigidbody2D rb;
	private Rigidbody2D dev;
	public float devInitialSpeed {get; set;}						// Dev's speed orbiting the sun

	void Awake() 
	{
		rb = GetComponent<Rigidbody2D>();

		// Grab the slider component of the menu, and set this to devInitialSpeed
		Slider slider = GameObject.Find("Slider").GetComponent<Slider>();
		devInitialSpeed = slider.value;


	}

	//This trigger is set so that Dev rotates around the sun while inside the collider
	void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			dev = other.GetComponent<Rigidbody2D>();

			//Calculate the vector that points from Dev to Sun
			Vector2 relativePos = (dev.position - rb.position);

			//Based on the relative vector, this angle makes sure that Dev is perpendicular to the vector
			float angle = Mathf.Atan2(relativePos.y, relativePos.x) * Mathf.Rad2Deg;

			//This float is added to the angle to prevent dev from spiraling outwards
			float offset = devInitialSpeed / 5;

			//This is the direction to which Dev should rotate towards
			Quaternion rotate = Quaternion.AngleAxis(Mathf.Round(angle) + offset, Vector3.forward);

			Quaternion current = dev.transform.rotation;

			//Rotates Dev, and moves his position "upwards" relative to itself
			dev.transform.localRotation = Quaternion.RotateTowards(current, rotate, Time.deltaTime * 50000);
			dev.transform.Translate(Vector3.up * Time.deltaTime * devInitialSpeed);
		}
	

	}
}
